# Advent of Code 2019 - Solutions

https://adventofcode.com/2019

## Puzzles

| Day  | Done  | Puzzle |
| :--- | :---: | :----: |
| 1  | ✔ | [The Tyranny of the Rocket Equation](https://adventofcode.com/2019/day/1) \[[solution](src/puzzle/day_1.rs)\]   |
| 2  | ✔ | [1202 Program Alarm](https://adventofcode.com/2019/day/2) \[[solution](src/puzzle/day_2.rs)\]                   |
| 3  | ✔ | [Crossed Wires](https://adventofcode.com/2019/day/3) \[[solution](src/puzzle/day_3.rs)\]                        |
| 4  | ✔ | [Secure Container](https://adventofcode.com/2019/day/4) \[[solution](src/puzzle/day_4.rs)\]                     |
| 5  | ✔ | [Sunny with a Chance of Asteroids](https://adventofcode.com/2019/day/5) \[[solution](src/puzzle/day_5.rs)\]     |
| 6  | ✔ | [Universal Orbit Map](https://adventofcode.com/2019/day/6) \[[solution](src/puzzle/day_6.rs)\]                  |
| 7  | ✘ | [Amplification Circuit](https://adventofcode.com/2019/day/7)                                                    |
| 8  | ✘ | [Space Image Format](https://adventofcode.com/2019/day/8)                                                       |
| 9  | ✘ | [Sensor Boost](https://adventofcode.com/2019/day/9)                                                             |
| 10 | ✘ | [Monitoring Station](https://adventofcode.com/2019/day/10)                                                      |
| 11 | ✘ | [Space Police](https://adventofcode.com/2019/day/11)                                                            |
| 12 | ✘ | [The N-Body Problem](https://adventofcode.com/2019/day/12)                                                      |
| 13 | ✘ | [Care Package](https://adventofcode.com/2019/day/13)                                                            |
| 14 | ✘ | [Space Stoichiometry](https://adventofcode.com/2019/day/14)                                                     |
| 15 | ✘ | [Oxygen System](https://adventofcode.com/2019/day/15)                                                           |
| 16 | ✘ | [Flawed Frequency Transmission](https://adventofcode.com/2019/day/16)                                           |
| 17 | ✘ | [Set and Forget](https://adventofcode.com/2019/day/17)                                                          |
| 18 | ✘ | [Many-Worlds Interpretation](https://adventofcode.com/2019/day/18)                                              |
| 19 | ✘ | [Tractor Beam](https://adventofcode.com/2019/day/19)                                                            |
| 20 | ✘ | [Donut Maze](https://adventofcode.com/2019/day/20)                                                              |
| 21 | ✘ | [Springdroid Adventure](https://adventofcode.com/2019/day/21)                                                   |
| 22 | ✘ | [Slam Shuffle](https://adventofcode.com/2019/day/22)                                                            |
| 23 | ✘ | [Category Six](https://adventofcode.com/2019/day/23)                                                            |
| 24 | ✘ | [Planet of Discord](https://adventofcode.com/2019/day/24)                                                       |
| 25 | ✘ | [Cryostasis](https://adventofcode.com/2019/day/25)                                                              |

## Build and Run

```bash
$ cargo run <day> # e.g. cargo run "day 3"
```
