// Copyright 2021 Lorenz Pahl
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

mod day_1;
mod day_2;
mod day_3;
mod day_4;
mod day_5;
mod day_6;

use regex::Regex;
use std::str::FromStr;

pub enum Puzzle {
    Day1,
    Day2,
    Day3,
    Day4,
    Day5,
    Day6,
}

impl FromStr for Puzzle {
    type Err = String;

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        let puzzle_pattern =
            Regex::new(r"^(?:day\s?)?((?:0?[1-9])|(?:1[0-9])|(?:2[0-5]))$").unwrap();

        match puzzle_pattern.captures(value.trim()) {
            None => Err(format!("{} is not a valid puzzle identifier", value)),
            Some(caps) => match caps.get(1) {
                None => Err(format!("{} is not a valid puzzle identifier", value)),
                Some(raw_day) => match raw_day.as_str().parse::<u8>() {
                    Err(_) => Err(format!(
                        "{} could not be parsed as a valid puzzle identifier",
                        raw_day.as_str()
                    )),
                    Ok(day) => match day {
                        1 => Ok(Puzzle::Day1),
                        2 => Ok(Puzzle::Day2),
                        3 => Ok(Puzzle::Day3),
                        4 => Ok(Puzzle::Day4),
                        5 => Ok(Puzzle::Day5),
                        6 => Ok(Puzzle::Day6),
                        _ => Err(format!("puzzle {} has not yet been solved :(", day)),
                    },
                },
            },
        }
    }
}

pub fn run(puzzle: Puzzle) {
    match puzzle {
        Puzzle::Day1 => day_1::run(),
        Puzzle::Day2 => day_2::run(),
        Puzzle::Day3 => day_3::run(),
        Puzzle::Day4 => day_4::run(),
        Puzzle::Day5 => day_5::run(),
        Puzzle::Day6 => day_6::run(),
    }
}
