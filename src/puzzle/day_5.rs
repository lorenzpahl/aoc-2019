// Copyright 2022 Lorenz Pahl
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use intcode_computer::*;

/// --- Day 5: Sunny with a Chance of Asteroids ---
///
/// * Part I: 12440243
/// * Part II: 15486302
pub fn run() {
    let program_values = puzzle_input();
    let mut computer = IntcodeComputer::from(program_values);
    match computer.execute() {
        Ok(_) => println!("Done!"),
        Err(msg) => println!("{}", msg),
    }
}

fn puzzle_input() -> Vec<i32> {
    let input = "3,225,1,225,6,6,1100,1,238,225,104,0,1102,45,16,225,2,65,191,224,1001,224,-3172,224,4,224,102,8,223,223,1001,224,5,224,1,223,224,223,1102,90,55,225,101,77,143,224,101,-127,224,224,4,224,102,8,223,223,1001,224,7,224,1,223,224,223,1102,52,6,225,1101,65,90,225,1102,75,58,225,1102,53,17,224,1001,224,-901,224,4,224,1002,223,8,223,1001,224,3,224,1,224,223,223,1002,69,79,224,1001,224,-5135,224,4,224,1002,223,8,223,1001,224,5,224,1,224,223,223,102,48,40,224,1001,224,-2640,224,4,224,102,8,223,223,1001,224,1,224,1,224,223,223,1101,50,22,225,1001,218,29,224,101,-119,224,224,4,224,102,8,223,223,1001,224,2,224,1,223,224,223,1101,48,19,224,1001,224,-67,224,4,224,102,8,223,223,1001,224,6,224,1,223,224,223,1101,61,77,225,1,13,74,224,1001,224,-103,224,4,224,1002,223,8,223,101,3,224,224,1,224,223,223,1102,28,90,225,4,223,99,0,0,0,677,0,0,0,0,0,0,0,0,0,0,0,1105,0,99999,1105,227,247,1105,1,99999,1005,227,99999,1005,0,256,1105,1,99999,1106,227,99999,1106,0,265,1105,1,99999,1006,0,99999,1006,227,274,1105,1,99999,1105,1,280,1105,1,99999,1,225,225,225,1101,294,0,0,105,1,0,1105,1,99999,1106,0,300,1105,1,99999,1,225,225,225,1101,314,0,0,106,0,0,1105,1,99999,7,226,677,224,102,2,223,223,1005,224,329,1001,223,1,223,8,226,677,224,1002,223,2,223,1005,224,344,101,1,223,223,8,226,226,224,1002,223,2,223,1006,224,359,101,1,223,223,1008,677,226,224,1002,223,2,223,1005,224,374,1001,223,1,223,108,677,677,224,1002,223,2,223,1005,224,389,1001,223,1,223,1107,226,677,224,1002,223,2,223,1006,224,404,101,1,223,223,1008,226,226,224,102,2,223,223,1006,224,419,1001,223,1,223,7,677,226,224,1002,223,2,223,1005,224,434,101,1,223,223,1108,226,226,224,1002,223,2,223,1005,224,449,101,1,223,223,7,226,226,224,102,2,223,223,1005,224,464,101,1,223,223,108,677,226,224,102,2,223,223,1005,224,479,1001,223,1,223,1007,677,226,224,1002,223,2,223,1006,224,494,1001,223,1,223,1007,677,677,224,1002,223,2,223,1006,224,509,1001,223,1,223,107,677,677,224,1002,223,2,223,1005,224,524,101,1,223,223,1108,226,677,224,102,2,223,223,1006,224,539,1001,223,1,223,8,677,226,224,102,2,223,223,1005,224,554,101,1,223,223,1007,226,226,224,102,2,223,223,1006,224,569,1001,223,1,223,107,677,226,224,102,2,223,223,1005,224,584,1001,223,1,223,108,226,226,224,102,2,223,223,1006,224,599,1001,223,1,223,107,226,226,224,1002,223,2,223,1006,224,614,1001,223,1,223,1108,677,226,224,1002,223,2,223,1005,224,629,1001,223,1,223,1107,677,677,224,102,2,223,223,1005,224,644,1001,223,1,223,1008,677,677,224,102,2,223,223,1005,224,659,101,1,223,223,1107,677,226,224,1002,223,2,223,1006,224,674,101,1,223,223,4,223,99,226";
    input
        .split(',')
        .map(|text| text.trim().parse::<i32>().unwrap())
        .collect()
}

mod intcode_computer {
    use std::io;

    #[derive(PartialEq)]
    enum ParameterMode {
        Position,
        Immediate,
    }

    impl ParameterMode {
        fn from(parameter_mode: u8) -> Result<ParameterMode, String> {
            match parameter_mode {
                0 => Ok(ParameterMode::Position),
                1 => Ok(ParameterMode::Immediate),
                _ => Err(format!("unsupported parameter mode {}", parameter_mode)),
            }
        }
    }

    struct Parameter {
        mode: ParameterMode,
        value: i32,
    }

    struct ParsableDigits {
        digits: Vec<u8>,
    }

    impl ParsableDigits {
        fn of(value: u32) -> ParsableDigits {
            ParsableDigits {
                digits: decompose_number(value),
            }
        }

        fn opcode(&self) -> u32 {
            if self.digits.len() < 2 {
                *self.digits.get(0).expect("opcode expected") as u32
            } else {
                let ones_digit = self.digits.get(self.digits.len() - 1).unwrap();
                let tens_digit = self.digits.get(self.digits.len() - 2).unwrap();
                as_number(*tens_digit, *ones_digit)
            }
        }

        fn parameter_mode(&self, parameter_pos: usize) -> Result<ParameterMode, String> {
            if self.digits.len() < 3 {
                Ok(ParameterMode::Position)
            } else {
                let mode_offset = self.digits.len() - 3;
                if parameter_pos > mode_offset {
                    Ok(ParameterMode::Position)
                } else {
                    match self.digits.get(mode_offset - parameter_pos) {
                        None => Ok(ParameterMode::Position),
                        Some(digit) => match ParameterMode::from(*digit) {
                            Err(msg) => Err(msg),
                            ok => ok,
                        },
                    }
                }
            }
        }
    }

    enum Instruction {
        Add {
            lhs: Parameter,
            rhs: Parameter,
            out: Parameter,
        },
        Mul {
            lhs: Parameter,
            rhs: Parameter,
            out: Parameter,
        },
        In {
            sink: Parameter,
        },
        Out {
            source: Parameter,
        },
        JumpIfTrue {
            lhs: Parameter,
            rhs: Parameter,
        },
        JumpIfFalse {
            lhs: Parameter,
            rhs: Parameter,
        },
        LessThan {
            lhs: Parameter,
            rhs: Parameter,
            out: Parameter,
        },
        Eq {
            lhs: Parameter,
            rhs: Parameter,
            out: Parameter,
        },
        Halt,
    }

    impl Instruction {
        fn from(instruction_pointer: usize, program: &Vec<i32>) -> Result<Instruction, String> {
            match program.get(instruction_pointer) {
                None => Err(format!("no opcode at position {}", instruction_pointer)),
                Some(value) if *value < 0 => {
                    Err(format!("invalid value at position {}", instruction_pointer))
                }
                Some(value) if *value >= 0 => {
                    let digits = ParsableDigits::of(*value as u32);
                    match digits.opcode() {
                        1 => parse_binary_op(instruction_pointer, digits, program)
                            .map(|(lhs, rhs, out)| Instruction::Add { lhs, rhs, out }),
                        2 => parse_binary_op(instruction_pointer, digits, program)
                            .map(|(lhs, rhs, out)| Instruction::Mul { lhs, rhs, out }),
                        3 => {
                            match (
                                digits.parameter_mode(0),
                                program.get(instruction_pointer + 1),
                            ) {
                                (Ok(mode @ ParameterMode::Position), Some(value)) => {
                                    Ok(Instruction::In {
                                        sink: Parameter {
                                            mode,
                                            value: *value,
                                        },
                                    })
                                }
                                _ => Err(String::from("parameter required for input instruction")),
                            }
                        }
                        4 => {
                            match (
                                digits.parameter_mode(0),
                                program.get(instruction_pointer + 1),
                            ) {
                                (Ok(mode), Some(value)) => Ok(Instruction::Out {
                                    source: Parameter {
                                        mode,
                                        value: *value,
                                    },
                                }),
                                _ => Err(String::from("parameter required for output instruction")),
                            }
                        }
                        5 => {
                            match (
                                (
                                    digits.parameter_mode(0),
                                    program.get(instruction_pointer + 1),
                                ),
                                (
                                    digits.parameter_mode(1),
                                    program.get(instruction_pointer + 2),
                                ),
                            ) {
                                ((Ok(mode_a), Some(a)), (Ok(mode_b), Some(b))) => {
                                    Ok(Instruction::JumpIfTrue {
                                        lhs: Parameter {
                                            mode: mode_a,
                                            value: *a,
                                        },
                                        rhs: Parameter {
                                            mode: mode_b,
                                            value: *b,
                                        },
                                    })
                                }
                                _ => Err(String::from("parameter required for jump instruction")),
                            }
                        }
                        6 => {
                            match (
                                (
                                    digits.parameter_mode(0),
                                    program.get(instruction_pointer + 1),
                                ),
                                (
                                    digits.parameter_mode(1),
                                    program.get(instruction_pointer + 2),
                                ),
                            ) {
                                ((Ok(mode_a), Some(a)), (Ok(mode_b), Some(b))) => {
                                    Ok(Instruction::JumpIfFalse {
                                        lhs: Parameter {
                                            mode: mode_a,
                                            value: *a,
                                        },
                                        rhs: Parameter {
                                            mode: mode_b,
                                            value: *b,
                                        },
                                    })
                                }
                                _ => Err(String::from("parameter required for jump instruction")),
                            }
                        }
                        7 => parse_binary_op(instruction_pointer, digits, program)
                            .map(|(lhs, rhs, out)| Instruction::LessThan { lhs, rhs, out }),
                        8 => parse_binary_op(instruction_pointer, digits, program)
                            .map(|(lhs, rhs, out)| Instruction::Eq { lhs, rhs, out }),
                        99 => Ok(Instruction::Halt),
                        _ => Err(format!("found unknown opcode {}", digits.opcode())),
                    }
                }
                Some(invalid_value) => Err(format!(
                    "invalid value {} at opcode position {}",
                    invalid_value, instruction_pointer
                )),
            }
        }

        fn arity(&self) -> u8 {
            match self {
                Instruction::Halt => 1,
                Instruction::Add {
                    lhs: _,
                    rhs: _,
                    out: _,
                } => 4,
                Instruction::Mul {
                    lhs: _,
                    rhs: _,
                    out: _,
                } => 4,
                Instruction::In { sink: _ } => 2,
                Instruction::Out { source: _ } => 2,
                Instruction::JumpIfTrue { lhs: _, rhs: _ } => 3,
                Instruction::JumpIfFalse { lhs: _, rhs: _ } => 3,
                Instruction::LessThan {
                    lhs: _,
                    rhs: _,
                    out: _,
                } => 4,
                Instruction::Eq {
                    lhs: _,
                    rhs: _,
                    out: _,
                } => 4,
            }
        }

        fn eval_on(
            &self,
            instruction_pointer: &mut usize,
            program: &mut Vec<i32>,
        ) -> Result<(), String> {
            match self {
                Instruction::Halt => Ok(()),
                add_instruction @ Instruction::Add { lhs, rhs, out } => {
                    eval_binary_op(lhs, rhs, out, program, |a, b| a + b).and_then(|_| {
                        *instruction_pointer += add_instruction.arity() as usize;
                        Ok(())
                    })
                }
                mul_instruction @ Instruction::Mul { lhs, rhs, out } => {
                    eval_binary_op(lhs, rhs, out, program, |a, b| a * b).and_then(|_| {
                        *instruction_pointer += mul_instruction.arity() as usize;
                        Ok(())
                    })
                }
                in_instruction @ Instruction::In { sink } => {
                    if sink.mode != ParameterMode::Position {
                        return Err(String::from("invalid parameter mode"));
                    }

                    let mut buffer = String::new();
                    let stdin = io::stdin();
                    println!("Please enter a number:");
                    match stdin.read_line(&mut buffer) {
                        Err(err) => Err(err.to_string()),
                        Ok(_) => match buffer.trim().parse::<i32>() {
                            Err(err) => Err(err.to_string()),
                            Ok(value) => match program.get_mut(sink.value as usize) {
                                None => Err(String::from("invalid program values")),
                                Some(store) => {
                                    *store = value;
                                    *instruction_pointer += in_instruction.arity() as usize;
                                    Ok(())
                                }
                            },
                        },
                    }
                }
                out_instruction @ Instruction::Out { source } => {
                    let output_item = match source.mode {
                        ParameterMode::Immediate => Some(&source.value),
                        ParameterMode::Position => match program.get(source.value as usize) {
                            None => None,
                            item => item,
                        },
                    };

                    match output_item {
                        None => Err(format!("unable to read value at {}", source.value)),
                        Some(item) => {
                            println!("{}", item);
                            *instruction_pointer += out_instruction.arity() as usize;
                            Ok(())
                        }
                    }
                }
                jump_if_true_instruction @ Instruction::JumpIfTrue { lhs, rhs } => {
                    let lhs_operand = match lhs.mode {
                        ParameterMode::Immediate => Some(&lhs.value),
                        ParameterMode::Position => match program.get(lhs.value as usize) {
                            None => None,
                            item => item,
                        },
                    };

                    let rhs_operand = match rhs.mode {
                        ParameterMode::Immediate => Some(&rhs.value),
                        ParameterMode::Position => match program.get(rhs.value as usize) {
                            None => None,
                            item => item,
                        },
                    };

                    match (lhs_operand, rhs_operand) {
                        (Some(a), Some(b)) if *a != 0 => {
                            *instruction_pointer = *b as usize;
                            Ok(())
                        }
                        _ => {
                            *instruction_pointer += jump_if_true_instruction.arity() as usize;
                            Ok(())
                        }
                    }
                }
                jump_if_false_instruction @ Instruction::JumpIfFalse { lhs, rhs } => {
                    let lhs_operand = match lhs.mode {
                        ParameterMode::Immediate => Some(&lhs.value),
                        ParameterMode::Position => match program.get(lhs.value as usize) {
                            None => None,
                            item => item,
                        },
                    };

                    let rhs_operand = match rhs.mode {
                        ParameterMode::Immediate => Some(&rhs.value),
                        ParameterMode::Position => match program.get(rhs.value as usize) {
                            None => None,
                            item => item,
                        },
                    };

                    match (lhs_operand, rhs_operand) {
                        (Some(a), Some(b)) if *a == 0 => {
                            *instruction_pointer = *b as usize;
                            Ok(())
                        }
                        _ => {
                            *instruction_pointer += jump_if_false_instruction.arity() as usize;
                            Ok(())
                        }
                    }
                }
                less_than_instruction @ Instruction::LessThan { lhs, rhs, out } => {
                    eval_binary_op(lhs, rhs, out, program, |a, b| if a < b { 1 } else { 0 })
                        .and_then(|_| {
                            *instruction_pointer += less_than_instruction.arity() as usize;
                            Ok(())
                        })
                }
                eq_instruction @ Instruction::Eq { lhs, rhs, out } => {
                    eval_binary_op(lhs, rhs, out, program, |a, b| if a == b { 1 } else { 0 })
                        .and_then(|_| {
                            *instruction_pointer += eq_instruction.arity() as usize;
                            Ok(())
                        })
                }
            }
        }
    }

    fn eval_binary_op<F: FnOnce(&i32, &i32) -> i32>(
        lhs_param: &Parameter,
        rhs_param: &Parameter,
        out_param: &Parameter,
        program: &mut Vec<i32>,
        op: F,
    ) -> Result<(), String> {
        if out_param.mode != ParameterMode::Position {
            return Err(String::from("out parameter must be in immediate mode"));
        }

        let lhs: Result<&i32, String> = match lhs_param.mode {
            ParameterMode::Immediate => Ok(&lhs_param.value),
            ParameterMode::Position => {
                let lhs_address: Result<usize, std::num::TryFromIntError> =
                    lhs_param.value.try_into();
                if let Ok(address) = lhs_address {
                    program
                        .get(address)
                        .ok_or(String::from("invalid program values"))
                } else {
                    Err(format!("invalid address {}", lhs_param.value))
                }
            }
        };

        let rhs: Result<&i32, String> = match rhs_param.mode {
            ParameterMode::Immediate => Ok(&rhs_param.value),
            ParameterMode::Position => {
                let rhs_address: Result<usize, std::num::TryFromIntError> =
                    rhs_param.value.try_into();
                if let Ok(address) = rhs_address {
                    program
                        .get(address)
                        .ok_or(String::from("invalid program values"))
                } else {
                    Err(format!("invalid address {}", rhs_param.value))
                }
            }
        };

        let out: Result<usize, std::num::TryFromIntError> = out_param.value.try_into();

        match (lhs, rhs, out) {
            (Ok(lhs_operand), Ok(rhs_operand), Ok(out_address)) => {
                let result = op(lhs_operand, rhs_operand);
                if let Some(sink) = program.get_mut(out_address) {
                    *sink = result;
                    Ok(())
                } else {
                    Err(String::from("invalid program values"))
                }
            }
            _ => Err(String::from("unable to evaluate instruction")),
        }
    }

    fn parse_binary_op(
        instruction_pointer: usize,
        digits: ParsableDigits,
        program: &Vec<i32>,
    ) -> Result<(Parameter, Parameter, Parameter), String> {
        let lhs = (
            program.get(instruction_pointer + 1),
            digits.parameter_mode(0),
        );

        let rhs = (
            program.get(instruction_pointer + 2),
            digits.parameter_mode(1),
        );

        let out = (
            program.get(instruction_pointer + 3),
            digits.parameter_mode(3),
        );

        match (lhs, rhs, out) {
            (
                (Some(lhs_value), Ok(lhs_mode)),
                (Some(rhs_value), Ok(rhs_mode)),
                (Some(out_value), Ok(out_mode @ ParameterMode::Position)),
            ) => Ok((
                Parameter {
                    mode: lhs_mode,
                    value: *lhs_value,
                },
                Parameter {
                    mode: rhs_mode,
                    value: *rhs_value,
                },
                Parameter {
                    mode: out_mode,
                    value: *out_value,
                },
            )),
            _ => Err(String::from("invalid program input")),
        }
    }

    pub struct IntcodeComputer {
        instruction_pointer: usize,
        program_values: Vec<i32>,
    }

    impl IntcodeComputer {
        pub fn from(initial_program_values: Vec<i32>) -> IntcodeComputer {
            IntcodeComputer {
                instruction_pointer: 0,
                program_values: initial_program_values.to_vec(),
            }
        }

        pub fn execute(&mut self) -> Result<(), String> {
            while let Ok(next_instruction) =
                Instruction::from(self.instruction_pointer, &self.program_values)
            {
                match next_instruction {
                    Instruction::Halt => return Ok(()),
                    instruction => {
                        if let Err(msg) = instruction
                            .eval_on(&mut self.instruction_pointer, &mut self.program_values)
                        {
                            return Err(msg);
                        }
                    }
                }
            }

            Err(String::from("invalid program input"))
        }
    }

    pub fn decompose_number(x: u32) -> Vec<u8> {
        let mut digits: Vec<u8> = Vec::new();
        let mut num = x;
        loop {
            let digit = (num % 10) as u8;
            digits.insert(0, digit);
            if num < 10 {
                break;
            } else {
                num = num / 10;
            }
        }

        digits
    }

    pub fn as_number(tens_digit: u8, ones_digit: u8) -> u32 {
        assert!(tens_digit < 10);
        assert!(ones_digit < 10);

        (tens_digit as u32) * 10u32 + (ones_digit as u32)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn one_digit_number() {
        assert_eq!(as_number(0, 4), 4)
    }

    #[test]
    fn two_digit_number() {
        assert_eq!(as_number(9, 9), 99)
    }

    #[test]
    fn decompose_one_digit_number() {
        assert_eq!(decompose_number(8), Vec::from([8]))
    }

    #[test]
    fn decompose_multi_digit_number() {
        assert_eq!(
            decompose_number(987654321),
            Vec::from([9, 8, 7, 6, 5, 4, 3, 2, 1])
        )
    }
}
