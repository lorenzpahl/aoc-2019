// Copyright 2021 Lorenz Pahl
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// --- Day 1: The Tyranny of the Rocket Equation ---
///
/// * Part I: 3335787
/// * Part II: 5000812
pub fn run() {
    let mut total_fuel_requirement = 0;
    let mut extended_fuel_requirement = 0;
    for module_mass in puzzle_input() {
        total_fuel_requirement += fuel_requirement_for_mass(module_mass);
        extended_fuel_requirement += extended_fuel_requirement_for_mass(module_mass);
    }

    println!("Part I: the sum of the fuel requirements for all of the modules is {}.", total_fuel_requirement);
    println!("Part II: the sum of the fuel requirements for all of the modules is {}.", extended_fuel_requirement);
}

fn fuel_requirement_for_mass(module_mass: i32) -> i32 {
    module_mass / 3 - 2
}

fn extended_fuel_requirement_for_mass(module_mass: i32) -> i32 {
    let mut fuel_requirement = fuel_requirement_for_mass(module_mass);
    let mut extended_fuel_requirement = 0;
    while fuel_requirement > 0 {
        extended_fuel_requirement += fuel_requirement;
        fuel_requirement = fuel_requirement_for_mass(fuel_requirement);
    }

    extended_fuel_requirement
}

fn puzzle_input() -> Vec<i32> {
    "72713
     93795
     64596
     99366
     124304
     122702
     105674
     94104
     144795
     109412
     138753
     71738
     62172
     149671
     88232
     145707
     82617
     123357
     63980
     149016
     130921
     125863
     119405
     77839
     140354
     135213
     130550
     131981
     149301
     68884
     52555
     121036
     75237
     64339
     60612
     132912
     63164
     145198
     109252
     130024
     100738
     74890
     89784
     134474
     68815
     117283
     144774
     138017
     149989
     111506
     119737
     65330
     52388
     69698
     124990
     84232
     58016
     142321
     119731
     86914
     68524
     87708
     60776
     119259
     73429
     79486
     120369
     57007
     91514
     87226
     131770
     78170
     52871
     149060
     73804
     60034
     72519
     98065
     132526
     99660
     74854
     111912
     51232
     71499
     127629
     83807
     91061
     60988
     133493
     95170
     110661
     54486
     63241
     141111
     142244
     93120
     137257
     79822
     95849
     69878"
     .split('\n')
     .map(|text| text.trim())
     .map(|raw_module_mass| raw_module_mass.parse::<i32>().unwrap())
     .collect()
}
