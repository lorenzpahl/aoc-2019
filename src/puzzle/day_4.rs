// Copyright 2022 Lorenz Pahl
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// --- Day 4: Secure Container ---
///
/// * Part I: 1864
/// * Part II: 1258
pub fn run() {
    let password_count = Password::new([1, 3, 7, 7, 7, 7]).count();
    println!(
        "Part I: {} different passwords meet the given criteria.",
        password_count
    );

    let strict_password_count = Password::new([1, 3, 7, 7, 7, 7])
        .filter(|digits| check_matching_digits(digits))
        .count();

    println!(
        "Part II: {} different passwords meet the given criteria.",
        strict_password_count
    );
}

struct Password {
    digits: [u8; 6],
    done: bool,
}

impl Password {
    pub const MIN: u32 = 137_683;
    pub const MAX: u32 = 596_253;

    pub fn new(digits: [u8; 6]) -> Password {
        if !is_weakly_valid(&digits) {
            panic!("invalid password");
        }

        Password {
            digits: digits,
            done: false,
        }
    }
}

impl Iterator for Password {
    type Item = [u8; 6];

    fn next(&mut self) -> Option<Self::Item> {
        if self.done {
            return None;
        }

        let next = &mut self.digits.clone();
        loop {
            increment(next);
            if is_weakly_valid(next) || as_number(next) > Password::MAX {
                break;
            }
        }

        let it = Some(self.digits);
        if is_weakly_valid(next) {
            self.digits = next.clone();
        } else {
            self.done = true;
        }

        it
    }
}

fn is_weakly_valid(digits: &[u8; 6]) -> bool {
    match digits
        .windows(2)
        .fold((true, false), |acc, items| match items {
            [x, y] => {
                let (non_dec_seq, has_twin) = acc;
                (non_dec_seq && y >= x, has_twin || x == y)
            }
            _ => panic!(),
        }) {
        (non_dec_seq, has_twin) => {
            non_dec_seq && has_twin && {
                let num = as_number(digits);
                num >= Password::MIN && num <= Password::MAX
            }
        }
    }
}

fn check_matching_digits(digits: &[u8; 6]) -> bool {
    let mut group_count = 0;
    for i in 1..digits.len() {
        if digits[i - 1] == digits[i] {
            group_count += 1;
        } else if group_count == 1 {
            return true;
        } else {
            group_count = 0;
        }
    }

    group_count == 1
}

fn increment(digits: &mut [u8; 6]) {
    increment_at(digits, digits.len() - 1);
}

fn increment_at(digits: &mut [u8; 6], i: usize) {
    let num = digits[i];
    if num < 9 {
        digits[i] = num + 1;
        for j in (i + 1)..digits.len() {
            digits[j] = digits[i];
        }
    } else if i == 0 {
        panic!("maximum value reached");
    } else {
        increment_at(digits, i - 1);
    }
}

fn as_number(digits: &[u8; 6]) -> u32 {
    digits
        .into_iter()
        .rev()
        .enumerate()
        .fold(0, |num, item| match item {
            (i, digit) => num + (*digit as u32) * 10u32.pow(i as u32),
        })
}
