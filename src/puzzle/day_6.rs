// Copyright 2022 Lorenz Pahl
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::clone::Clone;
use std::cmp::Eq;
use std::cmp::PartialEq;
use std::collections::HashMap;
use std::collections::HashSet;
use std::hash::Hash;

use petgraph::algo::dijkstra;
use petgraph::graph::NodeIndex;
use petgraph::graph::UnGraph;

use regex::Regex;

/// --- Day 6: Universal Orbit Map ---
///
/// Part I: 154386
/// Part II: 346
pub fn run() {
    let map_data = parse_map_data();
    let checksum = calc_orbit_count_checksum(&map_data);
    println!(
        "Part I: the total number of direct and indirect orbits is {}.",
        checksum
    );

    let transfer_distance = calc_min_orbital_transfers(
        &map_data,
        &OrbitalObject {
            name: String::from("YOU"),
        },
        &OrbitalObject {
            name: String::from("SAN"),
        },
    );

    println!(
        "Part II: the minimum number of orbital transfers required is {}.",
        transfer_distance - 2
    );
}

fn calc_min_orbital_transfers(
    relationships: &Vec<OrbitalRelationship>,
    start: &OrbitalObject,
    stop: &OrbitalObject,
) -> u32 {
    let orbital_objects: HashSet<OrbitalObject> = relationships
        .into_iter()
        .flat_map(|relationship| {
            [
                relationship.center_object.clone(),
                relationship.orbiting_object.clone(),
            ]
        })
        .collect();

    let indices = 0..(orbital_objects.len() as u32);
    let indices_by_orbital_object: HashMap<&OrbitalObject, u32> =
        orbital_objects.iter().zip(indices).collect();

    let edges: Vec<(u32, u32)> = relationships
        .into_iter()
        .map(|relationship| {
            (
                indices_by_orbital_object[&relationship.center_object],
                indices_by_orbital_object[&relationship.orbiting_object],
            )
        })
        .collect();

    let orbital_graph = UnGraph::<u32, ()>::from_edges(edges);
    let stop_node: NodeIndex = indices_by_orbital_object[stop].into();
    let path_costs_by_node_id = dijkstra(
        &orbital_graph,
        indices_by_orbital_object[start].into(),
        Some(stop_node),
        |_| 1,
    );

    path_costs_by_node_id[&stop_node]
}

fn calc_orbit_count_checksum(relationships: &Vec<OrbitalRelationship>) -> u32 {
    let orbital_map: HashMap<OrbitalObject, OrbitalObject> = relationships
        .into_iter()
        .map(|relationship| {
            (
                relationship.orbiting_object.clone(),
                relationship.center_object.clone(),
            )
        })
        .collect();

    let mut total_checksum = 0;
    let mut cached_checksums: HashMap<&OrbitalObject, u32> = HashMap::new();
    for orbiting_object in orbital_map.keys() {
        let checksum = checksum_for(orbiting_object, &orbital_map, &cached_checksums);
        cached_checksums.insert(orbiting_object, checksum);
        total_checksum += checksum;
    }

    total_checksum
}

fn checksum_for(
    orbital_object: &OrbitalObject,
    orbital_map: &HashMap<OrbitalObject, OrbitalObject>,
    cached_checksums: &HashMap<&OrbitalObject, u32>,
) -> u32 {
    let mut checksum = 0;
    let mut curr_object = orbital_object;
    while let Some(center_object) = orbital_map.get(curr_object) {
        checksum += 1;
        if let Some(cached_checksum) = cached_checksums.get(center_object) {
            checksum += cached_checksum;
            break;
        } else {
            curr_object = center_object;
        }
    }

    checksum
}

fn parse_map_data() -> Vec<OrbitalRelationship> {
    let orbital_relationship_pattern = Regex::new(r"(.+)\)(.+)").unwrap();
    let raw_map = include_str!("../res/day_6.txt");
    raw_map
        .split('\n')
        .map(|text| text.trim())
        .filter(|text| !text.is_empty())
        .map(|orbital_relationship| {
            orbital_relationship_pattern
                .captures(orbital_relationship)
                .map(|orbital_relationship_capture| {
                    match (
                        orbital_relationship_capture.get(1),
                        orbital_relationship_capture.get(2),
                    ) {
                        (Some(left_object), Some(right_object)) => OrbitalRelationship {
                            center_object: OrbitalObject {
                                name: left_object.as_str().to_string(),
                            },
                            orbiting_object: OrbitalObject {
                                name: right_object.as_str().to_string(),
                            },
                        },
                        _ => panic!("invalid input"),
                    }
                })
                .unwrap()
        })
        .collect()
}

#[derive(PartialEq, Eq, Hash, Clone)]
struct OrbitalObject {
    name: String,
}

struct OrbitalRelationship {
    center_object: OrbitalObject,
    orbiting_object: OrbitalObject,
}
