// Copyright 2021 Lorenz Pahl
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use intcode_computer::IntcodeComputer;

/// --- Day 2: 1202 Program Alarm ---
///
/// * Part I: 3306701
/// * Part II: 7621
pub fn run() {
    let gravity_assist_program = puzzle_input();
    let mut computer = computer_with_values(12, 2, &gravity_assist_program);
    match computer.execute() {
        Ok(final_value) => println!("Part I: the value at position 0 is {}.", final_value),
        Err(msg) => println!("{}", msg),
    }

    match find_noun_verb_for(19_690_720, &gravity_assist_program) {
        None => println!("no result"),
        Some((noun, verb)) => println!(
            "Part II: noun {} and verb {} give the following result: {}.",
            noun,
            verb,
            100 * noun + verb
        ),
    }
}

fn find_noun_verb_for(expected_result: i32, program: &Vec<i32>) -> Option<(i32, i32)> {
    (0..100)
        .flat_map(|noun| (0..100).map(move |verb| (noun, verb)))
        .find(|(noun, verb)| {
            let mut computer = computer_with_values(*noun, *verb, program);
            match computer.execute() {
                Ok(value) => value == expected_result,
                _ => false,
            }
        })
}

fn computer_with_values(noun: i32, verb: i32, program: &Vec<i32>) -> IntcodeComputer {
    let mut modified_program = program.to_vec();
    if let Some(curr_noun) = modified_program.get_mut(1) {
        *curr_noun = noun;
    } else {
        panic!("incorrect input");
    }

    if let Some(curr_verb) = modified_program.get_mut(2) {
        *curr_verb = verb;
    } else {
        panic!("incorrect input");
    }

    IntcodeComputer::from(modified_program)
}

fn puzzle_input() -> Vec<i32> {
    let input = "1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,13,1,19,1,9,19,23,2,23,13,27,1,27,9,31,2,31,6,35,1,5,35,39,1,10,39,43,2,43,6,47,1,10,47,51,2,6,51,55,1,5,55,59,1,59,9,63,1,13,63,67,2,6,67,71,1,5,71,75,2,6,75,79,2,79,6,83,1,13,83,87,1,9,87,91,1,9,91,95,1,5,95,99,1,5,99,103,2,13,103,107,1,6,107,111,1,9,111,115,2,6,115,119,1,13,119,123,1,123,6,127,1,127,5,131,2,10,131,135,2,135,10,139,1,13,139,143,1,10,143,147,1,2,147,151,1,6,151,0,99,2,14,0,0";
    input
        .split(',')
        .map(|text| text.trim().parse::<i32>().unwrap())
        .collect()
}

mod intcode_computer {
    enum Instruction {
        Add(i32),
        Mul(i32),
        Halt(i32),
    }

    impl Instruction {
        fn opcode(&self) -> i32 {
            match self {
                Instruction::Add(_) => 1,
                Instruction::Mul(_) => 2,
                Instruction::Halt(_) => 99,
            }
        }

        fn arity(&self) -> i32 {
            match self {
                Instruction::Add(_) => 4,
                Instruction::Mul(_) => 4,
                Instruction::Halt(_) => 1,
            }
        }

        fn eval_on(&self, program: &mut Vec<i32>) -> Result<(), String> {
            match self {
                Instruction::Add(opcode_pos) => {
                    eval_add_instruction(self.opcode(), *opcode_pos, program)
                }
                Instruction::Mul(opcode_pos) => {
                    eval_mul_instruction(self.opcode(), *opcode_pos, program)
                }
                Instruction::Halt(opcode_pos) => {
                    eval_halt_instruction(self.opcode(), *opcode_pos, program)
                }
            }
        }
    }

    fn eval_binary_op<F: FnOnce(&i32, &i32) -> i32>(
        opcode: i32,
        opcode_pos: i32,
        op: F,
        program: &mut Vec<i32>,
    ) -> Result<(), String> {
        if let None = program
            .get(opcode_pos as usize)
            .filter(|actual_opcode| **actual_opcode == opcode)
        {
            return Err(format!(
                "Opcode at position {} should be {}",
                opcode_pos, opcode
            ));
        }

        match (
            program.get((opcode_pos + 1) as usize),
            program.get((opcode_pos + 2) as usize),
            program.get((opcode_pos + 3) as usize),
        ) {
            (Some(a), Some(b), Some(c)) => {
                if let (Some(x), Some(y)) = (program.get(*a as usize), program.get(*b as usize)) {
                    let result = op(x, y);
                    let result_idx = *c as usize;
                    if let Some(z) = program.get_mut(result_idx) {
                        *z = result;
                        Ok(())
                    } else {
                        Err(format!(
                            "can't store result of operation at index {}",
                            result_idx
                        ))
                    }
                } else {
                    Err(format!(
                        "can't perform operation at position {} for opcode {}",
                        opcode_pos, opcode
                    ))
                }
            }
            _ => Err(format!(
                "invalid program input for opcode {} at position {}",
                opcode, opcode_pos
            )),
        }
    }

    fn eval_add_instruction(
        opcode: i32,
        opcode_pos: i32,
        program: &mut Vec<i32>,
    ) -> Result<(), String> {
        eval_binary_op(opcode, opcode_pos, |x, y| x + y, program)
    }

    fn eval_mul_instruction(
        opcode: i32,
        opcode_pos: i32,
        program: &mut Vec<i32>,
    ) -> Result<(), String> {
        eval_binary_op(opcode, opcode_pos, |x, y| x * y, program)
    }

    fn eval_halt_instruction(
        opcode: i32,
        opcode_pos: i32,
        program: &mut Vec<i32>,
    ) -> Result<(), String> {
        if let None = program
            .get(opcode_pos as usize)
            .filter(|actual_opcode| **actual_opcode == opcode)
        {
            Err(format!(
                "Opcode at position {} should be {}",
                opcode_pos, opcode
            ))
        } else {
            Ok(())
        }
    }

    pub struct IntcodeComputer {
        instruction_pointer: i32,
        program_values: Vec<i32>,
    }

    impl IntcodeComputer {
        pub fn from(initial_program_values: Vec<i32>) -> IntcodeComputer {
            IntcodeComputer {
                instruction_pointer: 0,
                program_values: initial_program_values.to_vec(),
            }
        }

        pub fn execute(&mut self) -> Result<i32, String> {
            while let Some(curr_instruction) = self.curr_instruction() {
                match *curr_instruction {
                    Instruction::Halt(_) => {
                        return match self.get(0) {
                            Some(value) => Ok(*value),
                            None => Err(String::from("no value at index 0")),
                        }
                    }
                    add_inst @ Instruction::Add(_) => {
                        if let Err(msg) = add_inst.eval_on(&mut self.program_values) {
                            return Err(msg);
                        } else {
                            self.instruction_pointer += add_inst.arity()
                        }
                    }
                    mul_inst @ Instruction::Mul(_) => {
                        if let Err(msg) = mul_inst.eval_on(&mut self.program_values) {
                            return Err(msg);
                        } else {
                            self.instruction_pointer += mul_inst.arity()
                        }
                    }
                }
            }

            Err(String::from("unable to compute result"))
        }

        fn curr_instruction(&self) -> Option<Box<Instruction>> {
            match self.program_values.get(self.instruction_pointer as usize) {
                Some(1) => Some(Box::new(Instruction::Add(self.instruction_pointer))),
                Some(2) => Some(Box::new(Instruction::Mul(self.instruction_pointer))),
                Some(99) => Some(Box::new(Instruction::Halt(self.instruction_pointer))),
                _ => None,
            }
        }

        fn get(&self, idx: usize) -> Option<&i32> {
            self.program_values.get(idx)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn verify_example_1() -> Result<(), String> {
        let data = Vec::from([1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50]);
        let mut computer = IntcodeComputer::from(data);
        assert_eq!(computer.execute()?, 3500);
        Ok(())
    }

    #[test]
    fn verify_example_2() -> Result<(), String> {
        let data = Vec::from([1, 0, 0, 0, 99]);
        let mut computer = IntcodeComputer::from(data);
        assert_eq!(computer.execute()?, 2);
        Ok(())
    }

    #[test]
    fn verify_example_3() -> Result<(), String> {
        let data = Vec::from([2, 3, 0, 3, 99]);
        let mut computer = IntcodeComputer::from(data);
        assert_eq!(computer.execute()?, 2);
        Ok(())
    }

    #[test]
    fn verify_example_4() -> Result<(), String> {
        let data = Vec::from([2, 4, 4, 5, 99, 0]);
        let mut computer = IntcodeComputer::from(data);
        assert_eq!(computer.execute()?, 2);
        Ok(())
    }

    #[test]
    fn verify_example_5() -> Result<(), String> {
        let data = Vec::from([1, 1, 1, 4, 99, 5, 6, 0, 99]);
        let mut computer = IntcodeComputer::from(data);
        assert_eq!(computer.execute()?, 30);
        Ok(())
    }
}
