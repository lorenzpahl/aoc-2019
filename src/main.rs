// Copyright 2021 Lorenz Pahl
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

mod puzzle;

use std::env;
use std::str::FromStr;

use crate::puzzle::Puzzle;

fn main() {
    let args: Vec<String> = env::args().collect();
    match parse_input(&args) {
        Err(msg) => println!("{}", msg.as_str()),
        Ok(day) => puzzle::run(day),
    }
}

fn parse_input(args: &[String]) -> Result<Puzzle, String> {
    if args.len() < 2 {
        Err(String::from("Usage: cargo run <day>"))
    } else {
        Puzzle::from_str(&args[1])
    }
}
